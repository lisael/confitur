# Package

version       = "0.1.0"
author        = "bruno.dupuis"
description   = "Confitur configuration language"
license       = "GPL-3.0"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["confitur"]


# Dependencies

requires "nim >= 0.19.4"
