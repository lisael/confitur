import strutils, sequtils

type
  CftNodeKind* = enum
    cnkExpr,
    cnkInfix,
    cnkPrefix,
    cnkSymbol,
    cnkIdentifier,
    cnkPostfix,
    cnkSubscirption,
    cnkCallArgs,
    cnkCall,
    cnkSubscript,
    cnkInteger,
    cnkFloat,
    cnkString,
    cnkBool,
    cnkNone,
    cnkArray,
    cnkObject,
    cnkItem,

  CftNode* = ref object
    kind*: CftNodeKind
    match*: string
    children*: seq[CftNode]

const
  cnkMatcher = [cnkIdentifier, cnkInteger, cnkBool, cnkString]

proc `$`* (n: CftNode): string =
  result = $(n.kind) & ":"
  case n.kind
  of cnkMatcher:
    result.add " " & n.match
  else:
    discard
  for child in n.children:
    if child != nil:
      for line in splitLines($child):
        result.add "\n " & line
  return result

proc add*(n: CftNode, c: CftNode) =
  n.children.add(c)

proc `[]`*(n: CftNode, idx:int ): CftNode =
  return n.children[idx]

proc `[]`*[U, V](n: CftNode, slice: HSlice[U, V]): seq[CftNode] =
  return n.children[slice]

proc len*(n: CftNode): int =
  return n.children.len

proc setLen*(n: CftNode, len: int) =
  n.children.setLen len

iterator items*(n: CftNode): CftNode =
  for c in n.children: yield c

proc newCfNode(val: int):

