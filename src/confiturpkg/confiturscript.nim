import pegs, tables, strutils, sets, sequtils

import ./ast

var grammar = """
Expr <- W (Infix / Postfix) W

Infix <- Term1 ( V Op1 W Term1 )*
Term1 <- Term2 ( V Op2 W Term2 )*
Term2 <- Term3 ( V Op3 W Term3 )*
Term3 <- Term4 ( V Op4 W Term4 )*
Term4 <- Postfix

Op1 <- '|>'
Op2 <- ( '==' / ':' )
Op3 <- ( '+' / '-')
Op4 <- ( '*' / '/' )

Paren <- '(' W Expr W ')'

Postfix <- Prefixed / NotPrefixed
Prefixed <- Prefix Postfix
NotPrefixed <- Primary ( CallArgs / Subscription )*

Prefix <- ( '-' / '!' '!' / '!' ) V

CallArgs <- '(' V Expr? ( V ',' V Expr )* V ')'
Subscription <- '[' W Expr W ']'

Primary <- Literal / Symbol / Paren

Symbol <- Ident ( '.' Ident)*
Ident <- \ident
Literal <- Float / Integer / String / Bool / None
Bool <- 'true' / 'false'
True <- 'true' / True / 'yes'
False <- 'false' / False / 'no'
None <- 'None' / 'null' / 'nil'
Integer <- ( [1-9][0-9_]* / '0' )
Float <- Integer '.' [0-9_]*

String <- '"' StringChar* '"'
StringChar <- [^"\\] / '\\"' / '\\\\'

W <- \s*
V <- [ \\t]*

""".peg

const
  # these rules create those nodes.
  ruleGnk = {
    "Term1": cnkInfix,
    "Term2": cnkInfix,
    "Term3": cnkInfix,
    "Infix": cnkInfix,
    "Prefix": cnkIdentifier,
    "Prefixed": cnkPrefix,
    "Op1": cnkIdentifier,
    "Op2": cnkIdentifier,
    "Op3": cnkIdentifier,
    "Op4": cnkIdentifier,
    "Integer": cnkInteger,
    "Float": cnkFloat,
    "String": cnkString,
    "Symbol": cnkSymbol,
    "Ident": cnkIdentifier,
    "Postfix": cnkPostfix,
    "CallArgs": cnkCallArgs,
    "Subscription": cnkSubscirption,
    "Bool": cnkBool,
    "None": cnkNone,
  }.toTable

proc factorInfix*(n: CftNode) =
  if n.children.len <= 3:
    return
  var newnode = CftNode(kind: cnkInfix)
  newnode.children = n.children[2..^1]
  n.children.setLen 2
  newnode.factorInfix
  n.children.add newnode

proc factorPostfix*(n: CftNode) =
  if n.children.len == 1:
    return
  var newchildren: seq[CftNode] = @[]
  case n.children[1].kind
  of cnkCallArgs:
    newchildren.add(CftNode(kind: cnkCall, children: n.children[0..1]))
  of cnkSubscirption:
    newchildren.add(CftNode(kind: cnkSubscript, children: n.children[0..1]))
  else:
    assert(false)
  n.children = concat(newchildren, n.children[2..^1])
  factorPostfix(n)

proc parse_script*(src: string, debug=false): CftNode =
  result = CftNode(kind: cnkExpr)

  template dbg(stuff:untyped) =
    if debug:
      echo dbgIndent & $(stuff)

  var
    node_stack: seq[CftNode] = @[result]
    expecting = initSet[string]()
    tip = 0
    dbgIndent = ""

  expecting.incl("Program")

  let
    langParser = grammar.eventParser:
      pkNonTerminal:
        enter:
          if p.nt.name in ruleGnk:
            node_stack.add CftNode(kind:ruleGnk[p.nt.name])
            dbgIndent.add "  "
            dbg "Enter " & p.nt.name # & " " & $(start)
          if start == tip:
            expecting.incl(p.nt.name)
          elif start > tip:
            tip = start
            expecting.clear
            expecting.incl(p.nt.name)
        leave:
          dbg p.nt.name & " " & $(length)
          if p.nt.name in ruleGnk:
            dbg "Leave " & p.nt.name
            var node = node_stack[^1]
            node_stack.setLen node_stack.high
            dbgIndent = dbgIndent[0..^3]
            var parent = node_stack[^1]
            if length > 0:
              var
                matchStr: string
              matchStr = s.substr(start, start+length-1)
              dbg "Found " & p.nt.name & ": " & matchStr
              if node.kind == cnkIdentifier:
                node.match = matchStr.strip
              else:
                node.match = matchStr
              case node.kind
              of cnkInfix:
                if node.len == 1:
                  parent.add node[0]
                else:
                  node.factorInfix
                  parent.add node
              of cnkPostfix:
                node.factorPostfix
                if node.len == 1:
                  parent.add node[0]
                else:
                  parent.add node
              of cnkExpr:
                parent.add node[0]
              else:
                parent.add node
            else:
              discard
              # dbg "discarded "

  var pLen = langParser src
  if pLen < src.high:
    echo "### Parsing Error ###"
    echo "Error at char " & $(tip)
    echo "  Expecting:"
    for e in expecting:
      echo "   " & e
    echo "  Found:"
    echo src.substr(tip, src.high)

when isMainModule:
  parse("1 + 2")
