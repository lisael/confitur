import unittest, strutils

import confiturpkg/confiturscript
import confiturpkg/ast

template test_parser(desc, src, expected: string, debug=false) =
  test desc:
    var
      tree = parse_script(src, debug)
      stree = $(tree)
    if debug:
      echo stree
    check(stree == expected)

suite "Test script":
    test_parser( "Test literal int 1",
      """
      42
      """,
      """cnkExpr:
 cnkInteger: 42
      """.strip)

    test_parser( "Test literal int 2",
      """
      - 42
      """,
      """cnkExpr:
 cnkPrefix:
  cnkIdentifier: -
  cnkInteger: 42
      """.strip)

    test_parser( "Test literal string",
      """
      "hello \"World\""
      """,
      """cnkExpr:
 cnkString: "hello \"World\""
      """.strip)

    test_parser( "Test Infix 1",
      """
      1 + 2
      """,
      """cnkExpr:
 cnkInfix:
  cnkInteger: 1
  cnkIdentifier: +
  cnkInteger: 2
      """.strip)

    test_parser( "Test Infix 2",
      """
      1 + -2 * (3 - 2 + 3)
      """,
      """cnkExpr:
 cnkInfix:
  cnkInteger: 1
  cnkIdentifier: +
  cnkInfix:
   cnkPrefix:
    cnkIdentifier: -
    cnkInteger: 2
   cnkIdentifier: *
   cnkInfix:
    cnkInteger: 3
    cnkIdentifier: -
    cnkInfix:
     cnkInteger: 2
     cnkIdentifier: +
     cnkInteger: 3
      """.strip)

    test_parser( "Test call",
      """
      func(1, 2 +3 )
      """,
      """cnkExpr:
 cnkCall:
  cnkSymbol:
   cnkIdentifier: func
  cnkCallArgs:
   cnkInteger: 1
   cnkInfix:
    cnkInteger: 2
    cnkIdentifier: +
    cnkInteger: 3
      """.strip)
